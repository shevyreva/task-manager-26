package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    private final String NAME = "task-show-by-project-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW ALL TASKS BY PROJECT ID]");
        System.out.println("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @NotNull final List<Task> tasks = getProjectTaskService().findAllTaskByProjectId(userId, projectId);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
