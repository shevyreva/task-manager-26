package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserLockedCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "Lock user.";

    @NotNull
    private final String NAME = "user-lock";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("Enter user login:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().findByLogin(login).setLocked(true);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
